 
var puts = console.log

function print_maze(b, n) {
    for (var j = 0; j < n; j++) { 
        var s = ""
        for (var i = 0; i < n; i++) {
            if (b[j * n + i] == 1) s += "#"
            else if (b[j * n + i] == -1) s += "o"
            else s += " "
        }
        puts(s)
    }
}
function make_maze(n) {
    var dir = [1, -1, n, -n]
    var dir2 = [1, -1, n]
    var b = []
    for (var i = 0; i < n; i++)
        for (var j = 0; j < n; j++)
            b[i * n + j] = 0
    for (var i = 0; i < n; i++) {
        b[0 * n + i] = b[(n - 1) * n + i] = 1
        b[i * n + 0] = b[i * n + (n - 1)] = 1
    }
    for (var j = 2; j < n - 1; j += 2)
        for (var i = 2; i < n - 1; i += 2) {
            var idx = j * n + i
            b[idx] = 1;
            var edir = []
            var ddir = dir2
            if (j == 2) ddir = dir
            for (var id = 0; id < ddir.length; id++){
                if (b[idx + ddir[id]] == 0) edir.push(ddir[id])
            }
            b[idx + edir[Math.floor(Math.random() * edir.length)]] = 1
        }
    return b
}
function solve_maze(b, n) {
    var dir = [1, -1, n, -n]
    var start = 1 * n + 1
    var goal = (n - 2) * n + (n - 2)
    function dfs(pos) {
        b[pos] = -1
        if (pos == goal) throw "done"
        else {
            for (var id = 0; id < dir.length; id++)
                if (b[pos + dir[id]] == 0) dfs(pos + dir[id])
        }
        b[pos] = 0
    }
    try{
        dfs(start)
    } catch (msg) {
    }
    return b
}
var n = 25  // should be an odd number
print_maze(solve_maze(make_maze(n), n), n)
