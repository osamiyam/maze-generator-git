# coding: utf-8
import random

def print_maze(b, n):
    for j in range(n):
        s = ""
        for i in range(n):
            if b[j * n + i] == 1: s += "#"
            elif b[j * n + i] == -1: s += "o"
            else: s += " "
        print s
def make_maze(n):
    rand = random.Random()
    dir = [1, -1, n, -n]
    dir2 = [1, -1, n]
    b = [0] * (n * n)
    for i in range(n):
        b[0 * n + i] = b[(n - 1) * n + i] = \
        b[i * n + 0] = b[i * n + (n - 1)] = 1
    for j in range(2, n - 1, 2):
        for i in range(2, n - 1, 2):
            idx = j * n + i
            b[idx] = 1;
            edir = []
            for d in (dir if j == 2 else dir2):
                if b[idx + d] == 0: edir.append(d)
            b[idx + rand.choice(edir)] = 1
    return b
def solve_maze(b, n):
    dir = [1, -1, n, -n]
    start = 1 * n + 1
    goal = (n - 2) * n + (n - 2)
    def dfs(pos):
        b[pos] = -1
        if pos == goal: raise
        else:
            for d in dir:
                if b[pos + d] == 0: dfs(pos + d)
        b[pos] = 0
    try: dfs(start)
    except: pass
    return b

n = 25  # should be an odd number
print_maze(solve_maze(make_maze(n), n), n)
